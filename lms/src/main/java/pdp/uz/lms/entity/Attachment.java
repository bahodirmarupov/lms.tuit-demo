package pdp.uz.lms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pdp.uz.lms.entity.templates.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@Data@AllArgsConstructor@NoArgsConstructor
@Entity
public class Attachment extends AbsEntity {
    private String name;
    private String fileType;
    @OneToOne
    private AttachmentContent attachmentContent;
}
