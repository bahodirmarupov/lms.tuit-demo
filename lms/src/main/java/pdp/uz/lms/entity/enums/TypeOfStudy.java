package pdp.uz.lms.entity.enums;

public enum TypeOfStudy {
    FullTime,
    PartTime
}
