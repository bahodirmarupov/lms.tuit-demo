package pdp.uz.lms.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import pdp.uz.lms.entity.enums.Degree;
import pdp.uz.lms.entity.enums.Gender;
import pdp.uz.lms.entity.enums.TypeOfStudy;
import pdp.uz.lms.entity.templates.AbsEntity;

import javax.persistence.Entity;
import java.sql.Date;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class Student extends AbsEntity {
    private String firstName;
    private String lastName;
    private String middleName;
    private Date birthDate;
    private Gender gender;
    private String reytingDaftarcha;
    private String faculty;
    private String language;
    private Degree degree;
    private TypeOfStudy typeOfStudy;
    private Byte year;
    private String SpecialGroup;
    private Boolean hasStipend;
}
