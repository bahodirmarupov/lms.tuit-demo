package pdp.uz.lms.entity.enums;

public enum Degree {
    Bachelor,
    Master
}
